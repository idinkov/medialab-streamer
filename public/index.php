<?php
if (substr($_SERVER['HTTP_HOST'], 0, 4) !== 'www.' || $_SERVER['HTTP_X_FORWARDED_PROTO'] != 'https') {
    $host = str_replace("www.", "", $_SERVER['HTTP_HOST']);
	//header('Location: https://www.'.$host. $_SERVER['REQUEST_URI']);
	//exit;
}
?>

<!DOCTYPE html>
<html lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Game One TV - Pure Nostalgia</title>

    <link rel="stylesheet" type="text/css" href="css/jquery.mCustomScrollbar.css" media="all" /><!-- scroll in playlist -->
    <link rel="stylesheet" type="text/css" href="css/mvp.css" />
    <link rel="stylesheet" type="text/css" href="css/pollux.css" />

    <style type="text/css">

        #wrapper{
            margin-top: 5px;
        }

        #wrap{
            margin: 50px auto;
            padding: 10px;
            max-width: 900px;
            border:1px solid #333;
        }
        .field{
            margin: 10px;
            display: flex;
            flex-direction:row;
        }
        #source-title{
            font-size: 20px;
        }
        #source-type-wrap{
            display: flex;
            flex-direction:row;
        }
        .source-type-inner{
            margin-left: 20px;
            display: flex;
            justify-content: center;
            align-items:center;
        }
        #stream-source{
            padding: 0 0 0 5px!important;
            box-sizing: border-box;
            border: 1px solid #999;
            border-radius: 0 !important;
            color: #999 !important;
            box-shadow: none!important;
            line-height: 1!important;
            user-select: text;
            font-size: 13px;
        }
        #source-confirm{
            margin-left: 10px;
            padding-left: 5px;
            padding: 8px 12px;
            background: #03a9f4;
            color: #fff;
            cursor: pointer;
        }
        #source-confirm:hover{
            background: #2196f3;
        }
        label{
            cursor: pointer;
            margin-left: 5px;
        }



    </style>

    <script type="text/javascript" src="js/jquery-3.5.1.min.js"></script>
    <script type="text/javascript" src="js/new.js"></script>
    <script type="text/javascript">

        var player;
        jQuery(document).ready(function($) {

            var settings = {

                sourcePath:"",
                instanceName:"player1",
                activePlaylist:".playlist-video",
                activeItem:0,
                volume:0.5,
                autoPlay:true,
                randomPlay:false,
                loopingOn:true,
                mediaEndAction:"next",
                aspectRatio:1,
                facebookAppId:"",
                youtubeAppId:"AIzaSyCC0XF_oHTHr1Wkxg6SbQSQkOnyUjpjzR4",
                playlistOpened:true,
                playlistScrollTheme:"light-2",
                useKeyboardNavigationForPlayback:false,
                truncatePlaylistDescription:true,
                forceYoutubeChromeless:true,
                rightClickContextMenu:'custom',
                elementsVisibilityArr:[
                    {width:600, elements:['play', 'next', 'previous', 'seekbar', 'fullscreen', 'settings', 'share', 'volume', 'playlist', 'time', 'chapter']}
                ],
                showStreamVideoBitrateMenu:true,
                showStreamAudioBitrateMenu:true,

                skin:'aviva',
                navigationType:'',
                playlistPosition:'no-playlist',
                playlistStyle:'',
                navigationStyle:'',
                playlistAnimation:'',
                playerShadow:'',

            };

            var url = '_skin/'+settings.skin+'.txt';
            $("#wrapper").load(url, function(){
                $(this).find('.mvp-previous-toggle').remove()
                $(this).find('.mvp-next-toggle').remove()
                player = $(this).mvp(settings);
            });

        });

    </script>

</head>
<body>

<div id="wrapper"></div>


<!-- LIST OF PLAYLISTS -->

<div id="mvp-playlist-list">

    <div class="playlist-video">

        <div class="mvp-playlist-item" data-type="hls" data-path="hls/gameone.m3u8" data-title="Game One" data-description="Description here."></div>
        <!--
        <div class="mvp-playlist-item" data-type="dash" data-path="https://dash.akamaized.net/akamai/bbb_30fps/bbb_30fps.mpd" data-mp4="http://clips.vorwaerts-gmbh.de/big_buck_bunny.mp4" data-thumb="media/video/thumb/02.jpg" data-title="Dynamic Adaptive Streaming over HTTP" data-description="MPEG-DASH is the first adaptive bit-rate HTTP-based streaming solution that is an international standard.[1] MPEG-DASH should not be confused with a transport protocol — the transport protocol that MPEG-DASH uses is TCP."></div>-->

    </div>

</div>



</body>
</html>