#!/bin/bash
cd /home/$USER/medialab/medialab-streamer/docker/
docker stop medialab.streamer.php medialab.streamer.nginx
docker rm medialab.streamer.php medialab.streamer.nginx
docker-compose up -d --build
cd ../../../